package com.sun.commons.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sunhongmin
 * 封装ajax返回结构
 */
public class ActionUtils {

	//private static Map<String,Object> retmap = new HashMap<>();
	//success : status 0
	//fail: status -1
	
	/**
	 * 返回成功
	 * @param info 成功信息
	 * @param data 数据
	 * @return {status:200,info:"成功信息",data:数据}
	 */
	public static Map<String,Object> ajaxSuccess(String info,Object data){
		Map<String,Object> retmap = new HashMap<>(8);
		retmap.put("status", 200);
		retmap.put("info",info);
		retmap.put("data", data);
		return retmap;
	}
	
	/**
	 * 返回失败
	 * @param info 失败信息
	 * @param data 数据
	 * @return {status:-1,info:"失败信息",data:数据}
	 */
	public static Map<String,Object> ajaxFail(String info,Object data){
		Map<String,Object> retmap = new HashMap<>(8);
		retmap.put("status", -1);
		retmap.put("info", info);
		retmap.put("data", data);
		return retmap;
	}
	
	/**返回成功
	 * @param data
	 * @param msg
	 * @return
	 */
	public static Map<String,Object> ajaxSuccessMsg(Object data,String msg){
		Map<String,Object> retmap = new HashMap<>(8);
		retmap.put("status", 200);
		retmap.put("data", data);
		retmap.put("msg", msg);
		return retmap;
	}
	
	/**
	 * 返回失败
	 * @param data
	 * @param msg
	 * @return
	 */
	public static Map<String,Object> ajaxFailMsg(Object data,String msg){
		Map<String,Object> retmap = new HashMap<>(8);
		retmap.put("status", -1);
		retmap.put("data", data);
		retmap.put("msg", msg);
		return retmap;
	}
	
}
