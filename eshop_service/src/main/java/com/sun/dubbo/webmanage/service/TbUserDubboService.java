package com.sun.dubbo.webmanage.service;

import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbUser;

public interface TbUserDubboService {

	TbUser loadTbUserByUsernameAndPassword(String username,String password);
	
	boolean insertTbUser(TbUser tbUser)throws DaoException;
	
	boolean updateTbUser(TbUser tbUser);
}
