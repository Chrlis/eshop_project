package com.sun.dubbo.webmanage.service;

import java.util.List;

import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbContentCategory;

public interface TbContentCategoryDubboService {

	List<TbContentCategory> listContentCatByPid(long pid);
	
	TbContentCategory insertTbContentCategory(TbContentCategory tbContentCategory,Long pid)throws DaoException;
	int updateTbContentCategory(TbContentCategory tbContentCategory)throws DaoException;
	boolean deleteTbContentCategory(Long parentId,Long id)throws DaoException;
	
	long countChildCategoryByPid(long pid);
	
	TbContentCategory loadContentCategoryByPK(long pid);
	
}
