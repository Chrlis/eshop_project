#近期待完成

-sql调优优化 
-manage项目不能deploy,以及不能通过nginx跳转到manager.eshop.com
-商品搜索跳转商品详情，配置在a标签中的地址不能访问

#新建mapper查询商品与类别关联信息

-搭建mysql主从数据库，利用mycat实现分库分表

-mybatis generator 要可灵活配置是否在插入后返回主键  tablename-back,tablename

-前台添加选择“一周之内免登录按钮”
-登录页面重新排版一下
-passport\order\登录页与注册页抽取公用页面头

-密码MD5加密
-shiro+redis+dubbo实现单点登录

#solr更新日志表增加更新条目数字段，便于统计每次操作所影响的条目数
-优化商品搜索，增加商品热度列，默认按热度实时排序商品列表（商品热度权重：点击  +1  加购物车 +2 生成订单 +3）
-新建一个控制器，可以按分类查指定分类的商品

-后台管理：商品信息维护时商品图片应便于操作者修改

-启用多线程优化：使用线程池管理线程的创建，防止大量开启线程造成系统故障

-统一异常处理（发生异常应该跳转到异常也而不是直接显示错误信息）

-每个项目的页面当中有一些url是需要动态配置的，需要统一抽离出来（检查是否可以利用pageConfig实现）

--添加示例商品，更新readme介绍
--将项目中软件的安装、配置整理出来放到博客上，作为该项目的参考文档，也可以引流到项目中

#添加商品订单信息(使用mysql批量新增)
#添加到订单的商品信息从购物车上清除
#更新商品库存信息
#添加购物车  购物车信息放到redis中  key:${cart:}
20191103待完成
#后台商品信息修改---BUG上传多张图片只剩最新的一张
#商品solr同步  添加不同步   上下架删除时，更新时同步信息
#商品详情增加缓存 前台缓存  后台同步 item:{item_id}
#商品描述  前台缓存  后台同步   itemDesc:{item_id}
#商品规格参数  前台缓存  后台同步   itemParam:{item_id}
#商品规格参数拼接HTML
#redis+cookie单点登录
#如果未选择,需要添加一个监听器，监听session销毁时清除redis中的session信息
#search首页优化：分类框显示内容溢出
#自己项目开发的子模块，应该将其版本统一到父项目中，方便升级之后的统一维护  及spring配置通用化
#待优化项记在这里（后续改进）


商品推荐
PageHelper分页插件优化
考虑将dubbo升级到最新版本   支持jdk1.8
考虑将搜索引擎由solr转换为Elasticsearch
 将solr转为Elasticsearch可以提升性能，Elasticsearch更适合实时搜索（不考虑建索引的同时进行搜索，solr速度更快）
https://www.cnblogs.com/fosilzhou/articles/4629220.html 搜索引擎选择： Elasticsearch与Solr 搜索引擎选型调研文档